package com.example.layout

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.button).setOnClickListener{
            addsong(it)
        }
    }
    private fun addsong(view: View){
        val editText = findViewById<EditText>(R.id.Edit_song)
        val songTextView=findViewById<TextView>(R.id.text_song)

        songTextView.text = editText.text
        editText.visibility = View.GONE
        view.visibility = View.GONE
        songTextView.visibility = View.VISIBLE

        val inn = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inn.hideSoftInputFromWindow(view.windowToken,0)
    }
}